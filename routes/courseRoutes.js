const express = require('express');
const router = express.Router();

const {
	getAllCourses,
	createCourse,
	course,
	updateCourse,
	deleteCourse,
	activeCourse,
	courseStatus,
	createToken
} = require('./../controllers/courseControllers');

const {verify, decode, verifyAdmin} = require('./../auth');


//Create a route /create to create a new course, save the course in DB and return the document
    //Only admin can create a course
router.post('/create', verifyAdmin, async (req, res) => {
	try{
		await createCourse(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}

})



//Create a route "/" to get all the courses, return all documents found
    //both admin and regular user can access this route
router.get('/', verifyAdmin, async (req, res) => {

	try{
		await getAllCourses().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})





//Create a route "/isActive" to get all active courses, return all documents found
    //both admin and regular user can access this route
router.get('/isActive', verifyAdmin, async (req, res) => {
	try{
		await activeCourse(req.body.courseName).then(result => res.send(result))
	}catch(error){
		res.status(500).json(error)
	}
})


//Create a route "/:courseId" to retrieve a specific course, save the course in DB and return the document
    //both admin and regular user can access this route
router.get('/:courseId', verifyAdmin, async (req, res) => {
	const courseId = decode(req.headers.authorization).id

	try{
		course(courseId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//Create a route "/:courseId/update" to update course info, return the updated document
    //Only admin can update a course
router.patch('/:courseId/update', verifyAdmin, async (req, res) => {
	console.log( decode(req.headers.authorization).id )
	console.log(req.body.password)

	const courseId = decode(req.headers.authorization).id
	try{
		await updateCourse(userId, req.body.password).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})



//Create a route "/:courseId/unArchive" to update isActive to true, return updated document
    //Only admin can update a course
router.patch('/:courseId/unArchive', verifyAdmin, async (req, res) => {
	try{
		await courseStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})





//Create a route "/:courseId/archive" to update isActive to false, return updated document
    //Only admin can update a course
router.patch('/:courseId/archive', verifyAdmin, async (req, res) => {
	try{
		await courseStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




//Create a route "/:id/delete-course" to delete a courses, return true if success
    //Only admin can delete a course
router.delete('/:id/delete-course', verifyAdmin, async (req, res) => {
	try{
		deleteCourse(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})





//Export the router module to be used in index.js file
module.exports = router;
